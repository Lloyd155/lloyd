#!/bin/bash

myTex='./*.tex'
wordCount='./*.pdf'

for eachFile in $myTex
do 
	pdflatex $eachFile
done

rm *.aux
rm *.log
rm *.bib

